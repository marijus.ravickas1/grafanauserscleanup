module mambu/grafanacleanup

go 1.19

require github.com/grafana/grafana-api-golang-client v0.18.2

require github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
