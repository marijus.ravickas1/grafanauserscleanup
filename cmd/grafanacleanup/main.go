package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	gapi "github.com/grafana/grafana-api-golang-client"
)

func main() {

	var baseUrl string = os.Getenv("GRAFANA_ADDRESS")
	if baseUrl == "" {
		fmt.Println(errors.New("env variable GRAFANA_ADDRESS is not defined"))
		os.Exit(1)
	}

	var token string = os.Getenv("GRAFANA_TOKEN")
	if baseUrl == "" {
		fmt.Println(errors.New("env variable GRAFANA_TOKEN is not defined"))
		os.Exit(1)
	}

	var config gapi.Config
	config.APIKey = token

	var client, _ = gapi.New(baseUrl, config)

	var users, _ = client.Users()
	var today = time.Now()
	var expirePeriod = 90 * 24 * time.Hour

	var usersToDelete []gapi.UserSearch

	for _, user := range users {
		if user.Email == "admin@localhost" {
			fmt.Println("Skipping admin@localhost")
			continue
		}

		if today.After(user.LastSeenAt.Add(expirePeriod)) {
			fmt.Printf("Username %s will be removed as last seen more than 3 months ago (%s) \n", user.Email, user.LastSeenAtAge)
			usersToDelete = append(usersToDelete, user)
		}
	}

	if os.Getenv("GRAFANA_USERS_CLEANUP") == "TRUE" {
		for _, user := range usersToDelete {
			err := client.DeleteUser(user.ID)
			if err != nil {
				fmt.Println(err)
				continue
			}
			fmt.Printf("Deleted user %s \n", user.Email)
		}
	}

}
